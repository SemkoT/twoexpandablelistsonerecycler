package pl.com.twoexpandablelistsonerecycler.firstadapter

data class First(val firstText: String, val secondText: String)