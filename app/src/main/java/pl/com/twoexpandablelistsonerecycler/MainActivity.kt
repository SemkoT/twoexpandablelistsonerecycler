package pl.com.twoexpandablelistsonerecycler

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import pl.com.twoexpandablelistsonerecycler.expandablelists.ExpandableListsAdapter
import pl.com.twoexpandablelistsonerecycler.expandablelists.PositionClickListener
import pl.com.twoexpandablelistsonerecycler.firstadapter.First
import pl.com.twoexpandablelistsonerecycler.firstadapter.FirstAdapter

class MainActivity : AppCompatActivity(), PositionClickListener {
    private var list: MutableList<ListAdapter<Any, RecyclerView.ViewHolder>>? = mutableListOf()
    private var adapter = ExpandableListsAdapter(this)
    private var firstAdapter: FirstAdapter = FirstAdapter()
    private var firstList = mutableListOf(
        First("First", "First"),
        First("Second", "Second"),
        First("Third", "Third"),
        First("Fourth", "Fourth"),
        First("Fifth", "Fifth"),
        First("Sixth", "Sixth")
    )
    private var secondList = mutableListOf(
        First("First", "First"),
        First("Second", "Second"),
        First("Third", "Third"),
        First("Fourth", "Fourth"),
        First("Fifth", "Fifth"),
        First("Sixth", "Sixth")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recycler.adapter = adapter
        inflateList()
    }

    private fun inflateList() {
        firstAdapter?.submitList(firstList)
        list?.add(firstAdapter)
        adapter.submitList(list)
    }

    override fun onPositionClicked(position: Int) {
    }

    override fun onAddSubitemClicked(position: Int) {
    }

    override fun onSubitemClicked(position: Int, subitemPosition: Int) {
    }

}
