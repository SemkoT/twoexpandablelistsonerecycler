package pl.com.twoexpandablelistsonerecycler.expandablelists

interface PositionClickListener {
    fun onPositionClicked(position: Int)
    fun onAddSubitemClicked(position: Int)
    fun onSubitemClicked(position: Int, subitemPosition: Int)
}