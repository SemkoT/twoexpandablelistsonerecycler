package pl.com.twoexpandablelistsonerecycler.expandablelists

import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import pl.com.twoexpandablelistsonerecycler.databinding.ExpandableListItemBinding

class ExpandableListViewHolder<V : RecyclerView.ViewHolder, T : ListAdapter<Any, V>>(
    private val binding: ExpandableListItemBinding,
    private val clickListener: PositionClickListener
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.headerText.setOnClickListener {
            clickListener.onPositionClicked(adapterPosition)
        }
    }

    fun bind(item: T) {
//        binding.item = item
        binding.nestedRecycler.adapter = item
    }
}