package pl.com.twoexpandablelistsonerecycler.expandablelists

import androidx.recyclerview.widget.DiffUtil

class ExpandableListDiffCallback : DiffUtil.ItemCallback<ExpandableList>() {
    override fun areItemsTheSame(
        oldItem: ExpandableList,
        newItem: ExpandableList
    ): Boolean {
        return oldItem.adapter == newItem.adapter
    }

    override fun areContentsTheSame(
        oldItem: ExpandableList,
        newItem: ExpandableList
    ): Boolean {
        return oldItem.adapter == newItem.adapter
    }
}