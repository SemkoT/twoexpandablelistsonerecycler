package pl.com.twoexpandablelistsonerecycler.expandablelists

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import pl.com.twoexpandablelistsonerecycler.databinding.ExpandableListItemBinding

class ExpandableListsAdapter(private val positionClickListener: PositionClickListener) :
    ListAdapter<ExpandableList, ExpandableListViewHolder>(ExpandableListDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpandableListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ExpandableListItemBinding.inflate(
            layoutInflater,
            parent,
            false
        )
        return ExpandableListViewHolder(binding, positionClickListener)
    }

    override fun onBindViewHolder(holder: ExpandableListViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}