package pl.com.twoexpandablelistsonerecycler.firstadapter

import androidx.recyclerview.widget.DiffUtil

class FirstDiffCallback : DiffUtil.ItemCallback<First>() {
    override fun areItemsTheSame(
        oldItem: First,
        newItem: First
    ): Boolean {
        return oldItem.firstText == newItem.firstText && oldItem.secondText == newItem.secondText
    }

    override fun areContentsTheSame(
        oldItem: First,
        newItem: First
    ): Boolean {
        return oldItem.firstText == newItem.firstText && oldItem.secondText == newItem.secondText
    }
}