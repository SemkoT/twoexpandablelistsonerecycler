package pl.com.twoexpandablelistsonerecycler.firstadapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import pl.com.twoexpandablelistsonerecycler.databinding.FirstListItemBinding

class FirstAdapter :
    ListAdapter<First, FirstViewHolder>(FirstDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FirstViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = FirstListItemBinding.inflate(
            layoutInflater,
            parent,
            false
        )
        return FirstViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FirstViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}