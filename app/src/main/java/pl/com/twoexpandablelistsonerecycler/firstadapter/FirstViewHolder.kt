package pl.com.twoexpandablelistsonerecycler.firstadapter

import androidx.recyclerview.widget.RecyclerView
import pl.com.twoexpandablelistsonerecycler.databinding.FirstListItemBinding

class FirstViewHolder(
    private val binding: FirstListItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: First) {
        binding.item = item
    }
}